#!/usr/bin/env bash

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ln -s "$BASE_DIR/gitconfig" ~/.gitconfig
ln -s "$BASE_DIR/vim" ~/.vim
ln -s "$BASE_DIR/tmux.conf" ~/.tmux.conf
